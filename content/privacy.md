---
title: "Privacy"
date: 2019-03-05T15:48:24+05:30
draft: false
---

Kebijakan privasi (privacy policy) ini dibuat untuk menyatakan komitmen kami dalam melindungi setiap informasi pengguna Aplikasi Pindahin. Adapun kebijakan privasi ini akan menjadi dasar atas setiap data yang diberikan dari pengguna yang meliputi perlakuan terhadap data yang diberikan.

Dengan menggunakan aplikasi pindahin, pengguna dianggap telah membaca serta memahami kebijakan privasi yang dibuat oleh Aplikasi Pindahin.

Kebijakan privasi ini dapat berubah sewaktu-waktu, untuk itu pengguna disarankan untuk membaca dan mencermati secara seksama setiap kebijakan privasi yang ada untuk mengetahui setiap perubahan yang ada. Dengan tetap menggunakan Aplikasi Pindahin, pengguna dianggap setuju dengan perubahan-perubahan yang ada pada kebijakan privasi.

 

Informasi Pengguna
------------------

Aplikasi Pindahin mengumpulkan informasi dari pengguna untuk mempermudah kelancaran dalam penggunaan aplikasi. Hal ini meliputi setiap informasi yang diminta pada saat mendaftar di aplikasi serta informasi perangkat yang dipakai untuk menggunakan aplikasi. Hal ini tidak meliputi informasi yang didapat oleh tautan (perlindungan informasi dari tautan merupakan tanggung jawab dari tautan tersebut).

Informasi dari pengguna yang mendaftar melalui Aplikasi Pindahin tidak akan dibagikan kepada semua pihak, kecuali pihak yang berwenang jika di butuhkan. Informasi yang diberikan kepada pihak berwenang berupa nama, no HP, E-mail, username serta IMEI.

Informasi yang didapat diupayakan untuk tetap aman dengan memberikan perlindungan secara elektronik.

Data berupa password disimpan dengan fitur enkripsi sehingga tidak dapat dibaca oleh pihak Aplikasi Pindahin.

Kerahasiaan password merupakan tanggung jawab pengguna, pihak Aplikasi Pindahin tidak bertanggung jawab jika terdapat kerugian yang terjadi akibat kecerobohan pengguna.

Pihak Aplikasi Pindahin tidak akan pernah meminta password atau kata sandi dari pengguna aplikasi.

 

Penggunaan Informasi Pengguna
------------------------------
Aplikasi Pindahin menggunakan informasi pengguna demi kelancaran proses aplikasi. Dalam hal ini, informasi yang didapat digunakan untuk mengolah informasi yang ada untuk menyediakan informasi yang lebih relevan pada pengguna aplikasi.

Selain itu, proses pengolahan informasi juga dapat dilakukan oleh Aplikasi Pindahin untuk mendapatkan informasi yang relevan sehingga dapat digunakan untuk memajukan Aplikasi Pindahin.

Informasi pribadi yang didapat dijamin kerahasiaannya untuk tidak disebarkan ke sembarang pihak. Hal ini tidak meliputi hasil dari pengolahan informasi yang dilakukan oleh Aplikasi Pindahin.

 

Mitra & Pelaku Usaha
--------------------
Aplikasi Pindahin memberikan wewenang untuk pengguna dapat memperjualbelikan produk dan jasa mereka, selama tidak bertentangan dengan hukum dan aturan yang berlaku, serta sesuai dengan peraturan perundang-undangan yang ada.

Aplikasi Pindahin akan berupaya untuk memaksimalkan pendapatan dan penjualan bagi Mitra dan pelaku usaha dalam bentuk Iklan dan promosi agar, Mitra dan Pelaku Usaha dapat memperoleh manfaat beriklan di Aplikasi Pindahin.

Bila Mitra & Pelaku Usaha dengan secara sengaja ataupun tidak sengaja melakukan pelanggaran hukum melalui aplikasi ini, maka Mitra & Pelaku Usaha dapat diproses hukum sesuai ketentuan yang berlaku dan pihak Aplikasi Pindahin tidak bertanggung jawab atas hal tersebut.

 

Versi Beta
-----------
Sampai saat ini, Aplikasi Pindahin masih dalam versi beta.

Hal ini berarti kesalahan-kesalahan dalam perlindungan atau pengolahan informasi mungkin saja terjadi. Namun, Aplikasi Pindahin akan berupaya untuk menjaga setiap proses pengolahan informasi yang ada sehingga informasi dari pengguna tetap bisa dijaga dan diolah dengan semestinya.

 

Kritik dan Saran
----------------
Aplikasi Pindahin sangat mengharapkan kritik dan saran dari pengguna aplikasi Pindahin. Adapun kritik dan saran yang diharapkan dapat disampaikan dengan santun dan demi kemajuan Aplikasi Pindahin.

Untuk setiap kritik dan saran dapat diberikan melalui e-mail ke cs@pindahin.net
