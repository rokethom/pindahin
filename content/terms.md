---
title: "Terms of Use"
date: 2019-03-05T15:47:17+05:30
draft: false
---

Syarat & ketentuan yang ditetapkan di bawah ini mengatur pemakaian jasa yang ditawarkan oleh Aplikasi PINDAHIN terkait penggunaan aplikasi. Pengguna disarankan membaca dengan seksama karena dapat berdampak kepada hak dan kewajiban Pengguna di bawah hukum.

Dengan mendaftar dan/atau menggunakan aplikasi Pindahin, maka pengguna dianggap telah membaca, mengerti, memahami dan menyetujui semua isi dalam Syarat & ketentuan. Syarat & ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam sebuah perjanjian yang sah antara Pengguna dengan Aplikasi Pindahin. Jika pengguna tidak menyetujui salah satu, sebagian, atau seluruh isi Syarat & ketentuan, maka pengguna tidak diperkenankan menggunakan layanan di aplikasi PINDAHIN.

MOHON ANDA MEMBACA DAN MEMATUHI KETENTUAN PENGGUNAAN KAMI INI SEBELUM MENGUNDUH APLIKASI ATAU MENGGUNAKAN LAYANAN KAMI UNTUK PERTAMA KALI. 

 
SYARAT DAN KETENTUAN 
 
1. Pelanggan wajib memberikan informasi yang benar dan lengkap mengenai jenis dan spesifikasi barang yang akan dikirimkan. 
PINDAHIN tidak menyediakan box khusus untuk pengiriman. Pelanggan bertanggung jawab untuk mengemas dengan layak barang yang akan dikirimkan. Untuk barang rapuh yang terbuat dari kaca, keramik dan termasuk juga kue, ice cream, makanan dan bunga segar, disarankan agar dikemas secara khusus. PINDAHIN tidak bertanggung jawab untuk kerusakan atau perubahan bentuk yang terjadi atas pengiriman barang-barang tersebut. 
 
2. Para pengemudi PINDAHIN adalah pengemudi yang sudah mendapatkan pengarahan untuk mengendarai kendaraannya dengan cara yang aman. Namun pelanggan yang menggunakan jasa transportasi bertanggung jawab untuk keselamatan mereka sendiri. Oleh karena itu jika anda merasa tidak nyaman dengan cara pengemudi PINDAHIN mengendarai motornya, harap untuk segera mengingatkan pengemudi agar lebih berhati-hati. 
 
3. PINDAHIN tidak memberikan layanan pengiriman untuk barang-barang dibawah ini: 
- Barang yang dilarang oleh pihak berwajib untuk dimiliki atau diedarkan. 
- Pengiriman barang dari dan ke penjara. 
- Binatang peliharaan. 
- Barang yang dimensinya lebih dari 50cm (panjang), 50cm (lebar), 50cm (tinggi) atau barang yang beratnya melebihi 20 kg untuk motor dan 50 kg untuk mobil. 
 
4. PINDAHIN tidak bertanggung jawab secara langsung untuk kecelakaan yang melibatkan driver PINDAHIN baik kerusakan terhadap kendaraan maupun luka badan yang disebabkan oleh kecelakaan tersebut. Tanggung jawab atas seluruh biaya serta tuntutan yang mungkin timbul atas kejadian tersebut akan menjadi tanggung jawab pribadi driver PINDAHIN. Jika ada informasi yang dapat membantu untuk proses investigasi seperti nama dan tempat tinggal driver serta nomor plat motor/mobil, maka PINDAHIN hanya dapat membantu sebagai mediator dalam mempertemukan kedua pihak untuk mencari penyelesaian masalah tersebut. 
 
5.  
 
 
A. HAL-HAL UMUM 
 
- PINDAHIN adalah suatu perusahaan yang didirikan berdasarkan hukum Negara Republik Indonesia. 
 
- Aplikasi ini merupakan aplikasi perangkat lunak yang berfungsi sebagai sarana untuk menemukan layanan dengan menggunakan sepeda motor atau mobil yang disediakan oleh pihak ketiga (driver). Aplikasi ini menawarkan informasi tentang layanan yang ditawarkan oleh driver. 
 
- Aplikasi ini memungkinkan anda untuk mengirimkan permintaan untuk suatu Layanan kepada driver. Penerima GPS - yang harus dipasang pada smartphone dimana anda telah mengunduh Aplikasi - mendeteksi lokasi anda dan mengirimkan informasi lokasi anda ke driver terkait. Driver memiliki kebijakan sendiri dan menyeluruh untuk menerima atau menolak setiap permintaan anda atas Layanan. Driver juga memiliki kebijakannya sendiri dan menyeluruh untuk memilih dan menerima arahan-arahan yang diberikan oleh Aplikasi tersebut. Jika driver menerima permintaan anda, Aplikasi akan memberitahu anda dan memberikan informasi mengenai driver (nama driver, nomor polisi kendaraannya, dan penilaian pelayanan pelanggan) dan kemampuan untuk menghubungi driver melalui telepon. 
 
- Kami akan melakukan semua upaya wajar untuk menghubungkan anda dengan driver untuk mendapatkan Layanan, tergantung kepada keberadaan driver di sekitar lokasi anda pada saat anda melakukan pemesanan Layanan. 
 
UNTUK MENGHINDARI KERAGU-RAGUAN, KAMI ADALAH PERUSAHAAN TEKNOLOGI, BUKAN PERUSAHAAN TRANSPORTASI ATAU KURIR DAN KAMI TIDAK MEMBERIKAN LAYANAN TRANSPORTASI ATAU KURIR. 
Kami tidak mempekerjakan driver dan kami tidak bertanggung jawab atas setiap tindakan dan/atau kelalaian driver. Aplikasi ini hanya merupakan sarana untuk memudahkan pencarian atas Layanan. Adalah tergantung pada driver untuk menawarkan Layanan kepada anda dan tergantung pada anda apakah anda akan menerima tawaran Layanan tersebut atau tidak. 
 
B. Ketentuan untuk Menggunakan Aplikasi 
 
- Anda menyatakan dan menjamin bahwa anda adalah individu yang secara hukum berhak untuk mengadakan perjanjian yang mengikat berdasarkan hukum Negara Republik Indonesia, khususnya Ketentuan Penggunaan, untuk menggunakan Aplikasi dan bahwa anda telah berusia minimal 21 tahun atau sudah menikah dan tidak berada di bawah perwalian. Jika tidak, kami atau driver terkait, berhak berdasarkan hukum untuk membatalkan perjanjian yang dibuat dengan anda. Anda selanjutnya menyatakan dan menjamin bahwa anda memiliki hak, wewenang dan kapasitas untuk menggunakan Layanan dan mematuhi Ketentuan Penggunaan. Jika anda mendaftarkan atas nama suatu badan hukum, anda juga menyatakan dan menjamin bahwa anda berwenang untuk mengadakan, dan mengikatkan diri entitas tersebut pada Ketentuan Penggunaan ini dan mendaftarkan untuk Layanan dan Aplikasi. 
 
- Kami mengumpulkan dan memproses informasi pribadi anda, seperti nama, surat elektronik (e-mail), dan nomor HP anda ketika anda mendaftar. Anda harus memberikan informasi yang akurat dan lengkap, memperbaharui informasi dan setuju untuk memberikan kepada kami bukti identitas apapun yang secara wajar dapat kami mintakan. Jika informasi pribadi yang anda berikan kepada kami ada yang berubah, misalnya, jika anda mengubah alamat e-mail, nomor HP, atau jika anda ingin membatalkan akun anda, mohon perbaharui rincian informasi Anda dengan mengirimkan permintaan anda kepada kami. Kami akan, sepanjang yang dapat kami lakukan, memberlakukan perubahan yang diminta tersebut dalam waktu lima belas (15) hari kerja sejak diterimanya pemberitahuan perubahan. 
 
- Anda hanya dapat menggunakan Aplikasi ketika anda telah mendaftar pada Aplikasi tersebut. Setelah anda berhasil mendaftarkan diri, Aplikasi akan memberikan anda suatu akun pribadi yang dapat diakses dengan kata sandi yang anda pilih. 
 
- Hanya anda yang dapat menggunakan akun anda sendiri dan anda berjanji untuk tidak memberikan wewenang kepada orang lain untuk menggunakan identitas anda atau menggunakan akun anda. Anda tidak dapat menyerahkan atau mengalihkan akun anda kepada pihak lain. Anda harus menjaga keamanan dan kerahasiaan kata sandi akun anda dan setiap identifikasi yang kami berikan kepada anda. Dalam hal terjadi pengungkapan atas kata sandi anda, dengan cara apapun, yang mengakibatkan setiap penggunaan yang tidak sah atau tanpa kewenangan atas akun atau identitas anda, pesanan yang diterima dari penggunaan yang tidak sah atau tanpa kewenangan tersebut masih akan dianggap sebagai pesanan yang sah, kecuali anda memberitahu kami tentang mengenai hal tersebut sebelum driver memberikan Layanan yang diminta. 
 
- Anda hanya dapat memiliki satu akun PINDAHIN 
 
- Informasi yang diberikan oleh Aplikasi tidak dapat diartikan sebagai suatu saran atau penawaran, keputusan untuk menggunakan driver sepenuhnya berada di tangan anda. Anda bebas untuk memilih untuk menggunakan driver lainnya. 
 
- Anda berjanji bahwa anda akan menggunakan Aplikasi hanya untuk tujuan yang dimaksud untuk mendapatkan Layanan. Anda tidak diperbolehkan untuk menyalahgunakan atau menggunakan Aplikasi untuk tujuan penipuan atau menyebabkan ketidaknyamanan kepada orang lain atau melakukan pemesanan palsu.
 
- Anda tidak diperkenankan untuk membahayakan, mengubah atau memodifikasi Aplikasi dan/atau Situs web atau mencoba untuk membahayakan, mengubah atau memodifikasi Aplikasi dan/atau Situs web dengan cara apapun.

- Kami tidak bertanggung jawab jika anda tidak memiliki perangkat yang sesuai atau jika anda telah mengunduh versi Aplikasi yang salah untuk perangkat anda. Kami berhak untuk melarang anda untuk menggunakan Aplikasi lebih lanjut jika anda menggunakan Aplikasi dengan perangkat yang tidak kompatibel/cocok atau tidak sah atau untuk tujuan lain selain daripada tujuan yang dimaksud untuk penggunaan Aplikasi ini.

- Anda berjanji bahwa anda hanya akan menggunakan suatu jalur akses yang diperbolehkan untuk anda gunakan. 
 
- Anda akan menjaga kerahasiaan dan tidak akan menyalahgunakan informasi yang anda terima dari penggunaan Aplikasi tersebut. Anda akan memperlakukan driver dengan hormat dan tidak akan terlibat dalam perilaku atau tindakan yang tidak sah, mengancam atau melecehkan ketika menggunakan layanan mereka. 
 
- Anda memahami dan setuju bahwa penggunaan Aplikasi oleh anda akan tunduk pula pada Kebijakan Privasi kami sebagaimana dapat diubah dari waktu ke waktu. Dengan menggunakan Aplikasi, Anda juga memberikan persetujuan sebagaimana dipersyaratkan berdasarkan Kebijakan Privasi kami. 
 
- Dengan memberikan informasi kepada kami, anda menyatakan bahwa anda berhak untuk memberikan kepada kami informasi yang akan kami gunakan dan berikan kepada driver. 
 
- Aplikasi tidak boleh dipergunakan untuk mencari Layanan untuk: 
 
1. mengangkut dan/atau memperoleh dan/atau membeli barang yang dilarang oleh pejabat yang berwenang atau barang yang memerlukan lisensi atau izin tertentu dari pejabat yang berwenang untuk dikirimkan. 
 
2. mengangkut barang dari dan ke penjara. 
 
3. membeli dan/atau mengangkut binatang peliharaan atau binatang lain. 
 
4. mengangkut barang-barang dengan dimensi lebih dari 50cm (panjang), 50cm (lebar), 50 cm (tinggi) atau barang-barang yang beratnya lebih dari 20 kg untuk motor dan 50 kg untuk mobil. 
 
5. membeli dan/atau mengangkut barang-barang ilegal atau berbahaya atau barang-barang curian, termasuk namun tidak terbatas pada barang-barang yang mengandung bahan berbahaya atau beracun, obat-obatan atau material terlarang/ilegal. 
 
6. membeli dan/atau mengangkut atau mengirimkan barang-barang berharga atau barang yang bernilai lebih dari Rp10.000.000 (sepuluh juta rupiah). 
 
- Anda harus memberikan kepada kami informasi yang akurat dan lengkap mengenai jenis, ukuran, spesifikasi dan/atau setiap karakteristik khusus dari makanan atau barang yang akan dikirimkan dengan driver. 
 
- Anda harus mengemas dengan benar barang-barang yang rapuh, seperti gelas, keramik, kue atau makanan, untuk pengiriman. Kami maupun driver tidak akan bertanggung jawab atas segala kerusakan, perubahan bentuk, pembengkokan, kadaluwarsa, pembusukan, bau, tumpahan atau ketidaklengkapan lain dari produk selama pengiriman yang disebabkan oleh kemasan atau pembungkusan yang kurang baik. 
 
- Driver dapat meminta anda untuk membuka dan menunjukkan bagian dalam suatu paket untuk memastikan bahwa isinya adalah seperti yang anda nyatakan. 
 
- Driver berhak untuk menolak pesanan anda jika menurut penilaian pribadi driver dia mungkin tidak dapat melakukan pengiriman barang. 
 
- Anda menyatakan bahwa anda telah, adalah, atau akan menjadi pemilik yang sah atas barang yang akan dikirimkan. Barang yang dikirim bukan barang curian atau diperoleh secara tidak sah dan anda setuju untuk menjaga, mengganti kerugian dan membebaskan kami dan kami tidak bertanggung jawab atas barang yang dikirimkan oleh driver yang mungkin melanggar peraturan perundang-undangan yang berlaku. 
 
- Kami dan/atau driver yang terkait berhak menolak untuk menerima pesanan Anda jika kami memiliki alasan yang wajar untuk mencurigai bahwa anda telah, atau dengan menerima pesanan dari anda, anda akan melanggar Ketentuan Penggunaan ini atau hukum dan peraturan perundang-undangan yang berlaku. 
 
- Kami dapat, berdasarkan kebijakan kami, memberikan promosi-promosi yang dapat ditukar untuk manfaat terkait dengan penggunaan Aplikasi. Anda setuju bahwa anda hanya akan menggunakan promosi tersebut sebagaimana promosi tersebut dimaksudkan dan tidak akan menyalahgunakan, menggandakan, menjual atau mengalihkan promosi tersebut dengan cara apapun. Anda juga memahami bahwa promosi tidak dapat ditukarkan dengan uang tunai dan dapat berakhir pada tanggal tertentu, bahkan sebelum anda menggunakannya. 
 
- Untuk ketentuan Layanan KURIR, anda menyatakan bahwa barang yang akan dibeli dan dkirimkan oleh driver adalah barang yang sah dan tidak melanggar hukum dan perundang-undangan yang berlaku dengan cara apapun. Anda setuju untuk menjaga, mengganti kerugian dan membebaskan kami dan kami tidak bertanggung jawab atas barang yang dibeli oleh driver atas nama anda dan diberikan kepada anda yang mungkin dapat melanggar hukum dan peraturan perundang-undangan yang berlaku. 
 
- Anda mengakui dan setuju untuk memberikan kepada driver kuasa dan wewenang untuk mengambil barang-barang atau makanan, melakukan suatu pembelian atas nama anda untuk setiap pemesanan. 
 
- Kami tidak menjamin ketersediaan barang pesanan di toko/rumah makan dan pengiriman barang-barang yang rapuh/mudah rusak seperti kue dan es krim. 
 
- Kami tidak bertanggung jawab atas kualitas makanan dan/atau barang yang disediakan oleh toko dan atau rumah makan. 
 
- Anda mengakui dan memahami bahwa harga makanan atau barang yang ditampilkan/dapatkan informasinya merupakan perkiraan dan dapat berubah dari waktu ke waktu. 
 
- Anda setuju dan mengakui bahwa anda akan membayar sesuai dengan tanda terima yang diterbitkan oleh rumah makan dan atau toko yang diserahkan oleh driver kepada Anda. 
 
- Kami atau driver tidak menjamin ketersediaan makanan atau barang di rumah makan atau toko. 
 
- Sehubungan dengan Layanan PIN-RIDE, anda berjanji bahwa anda akan mengenakan helm yang disediakan oleh driver anda selama perjalanan. Anda juga berjanji bahwa untuk setiap Layanan PIN-RIDE, hanya akan ada satu penumpang dari driver. Driver berhak untuk menolak atau membatalkan pesanan Layanan PIN-RIDE jika dia mengetahui bahwa jumlah penumpangnya akan lebih dari satu orang. 
 
- Mohon menginformasikan kepada kami jika anda tidak lagi memiliki kontrol atas akun anda, sebagai contoh akun anda diretas (hack) atau HP anda dicuri, sehingga kami dapat membatalkan akun anda dengan sebagaimana mestinya. Mohon diperhatikan bahwa anda bertanggung jawab atas penggunaan akun anda dan anda mungkin dapat dimintakan tanggung jawabnya meskipun jika akun anda tersebut disalahgunakan oleh orang lain. 
 
C. Pembayaran 
 
- Pengunduhan dan/atau penggunaan Aplikasi ini adalah bebas biaya. Kami dapat memperkenalkan pengenaan biaya untuk pengunduhan dan/atau penggunaan Aplikasi di setiap saat. Kami akan memberitahu anda tentang hal ini sebagaimana mestinya agar anda dapat memutuskan apakah anda ingin terus menggunakan Aplikasi tersebut atau tidak. Namun demikian, koneksi internet yang dibutuhkan untuk menggunakan Layanan, dan setiap biaya terkait (misalnya biaya data ponsel) yang ditimbulkan oleh penggunaan Layanan tersebut adalah tanggung jawab eksklusif anda dan semata-mata dibebankan kepada anda. 
 
- Tarif yang berlaku untuk Layanan oleh driver dapat ditemukan pada Situs dan melalui Aplikasi. Kami dapat mengubah atau memperbaharui tarif dari waktu ke waktu. Kami akan membantu driver untuk menghitung biaya berdasarkan pesanan anda dan memberitahu anda tentang biaya atas nama driver. 
 
- Anda setuju bahwa anda akan membayar Layanan yang diberikan kepada anda oleh driver secara penuh. 
 
- Pembayaran dapat dilakukan secara tunai atau dengan menggunakan SALDO APLIKASI anda. Semua pembayaran tunai harus dilakukan dalam Rupiah. SALDO APLIKASI dapat diperoleh dengan mentransfer jumlah yang anda inginkan ke rekening resmi PINDAHIN 
 
- SALDO APLIKASI bukan merupakan tabungan dan karena itu tidak termasuk dalam pengaturan oleh Lembaga Penjamin Simpanan Indonesia (LPS). 
 
- Jumlah maksimum SALDO anda adalah Rp1.000.000 (satu juta Rupiah) dan maksimum total nilai transaksi anda dalam sebulan adalah Rp10.000.000 (sepuluh puluh juta rupiah). 
 
- SALDO adalah dana Anda yang hanya dapat digunakan untuk pembayaran seluruh layanan kami. 
 
-SALDO dapat dari waktu ke waktu, menambah jumlah SALDO anda di akun anda sebagai bagian dari promosinya (misalnya, kode rujukan / referral code). Namun demikian, ini hanya suatu nilai yang setara dengan nilai uang berdasarkan peraturan perundang-undangan yang berlaku dan tidak dapat diubah menjadi uang elektronik (e-money). 
 
- Ketika anda meminta untuk menggunakan SALDO sebagai alat pembayaran atas Layanan, anda dengan ini mengakui dan setuju untuk memberikan kami kuasa dan kewenangan penuh untuk memotong saldo anda dan mentransfer uang atas nama anda untuk membayar Layanan yang disediakan oleh driver. 
 
- Makanan atau barang yang dipesan harus dibayar tunai pada saat penyerahan makanan atau barang jika nilai makanan atau barang di bawah Rp1.000.000 (satu juta Rupiah). 
 
- Setiap pemesanan layanan dengan total harga lebih dari Rp1.000.000, - (satu juta rupiah) harus dibayar tunai dimuka kepada driver sebelum pelaksanaan Layanan. 
 
D. JAMINAN 
 
Kami tidak memberikan pernyataan, jaminan atau garansi untuk dapat diandalkannya, ketepatan waktu, kualitas, kesesuaian, ketersediaan, akurasi atau kelengkapan dari Layanan, situs web dan atau perangkat lunak Aplikasi, termasuk namun tidak terbatas pada Layanan yang diperoleh atau berasal dari driver melalui penggunaan Aplikasi tersebut. Kami tidak menyatakan atau menjamin bahwa (a) penggunaan Layanan dan atau Aplikasi akan aman, tepat waktu, tanpa gangguan atau terbebas dari kesalahan atau beroperasi dengan kombinasi dengan perangkat keras lain, perangkat lunak, sistem atau data, (b) Layanan akan memenuhi kebutuhan atau harapan anda, (c) setiap data yang tersimpan akan akurat atau dapat diandalkan, (d) kualitas produk, layanan, informasi, atau bahan-bahan lain yang dibeli atau diperoleh oleh anda melalui Aplikasi akan memenuhi kebutuhan atau harapan anda, (e) kesalahan atau kecacatan dalam Aplikasi akan diperbaiki, (f) aplikasi atau server yang menyediakan Aplikasi terbebas dari virus atau komponen berbahaya lainnya, (g) Aplikasi melacak anda atau kendaraan yang digunakan oleh driver. Layanan disediakan untuk anda terbatas hanya pada dasar "sebagaimana adanya". Semua kondisi, pernyataan dan jaminan, baik tersurat, tersirat, yang diwajibkan oleh undang-undang atau sebaliknya, termasuk, namun tidak terbatas pada, jaminan yang tersirat mengenai jual beli, kesesuaian untuk tujuan tertentu, atau tidak adanya pelanggaran hak pihak ketiga, dengan ini dikecualikan dan dikesampingkan dengan batas tertinggi dan maksimum. Anda mengakui dan menyetujui bahwa seluruh risiko yang timbul dari penggunaan Layanan oleh anda tetap semata-mata dan sepenuhnya ada pada anda dan anda tidak akan memiliki hak untuk meminta ganti rugi apapun dari PINDAHIN. 
 
E. TANGGUNGJAWAB
